//
//  Tracker.swift
//  Tracker
//
//  Created by Ihwan on 08/07/21.
//


import Foundation

public protocol Tracker{
    func printEverything()
    func sayHi(_ name: String)
    func sayBye(_ name: String)
}

public class FirstTracker: Tracker {
    public init(){}
    public func printEverything() {
        print("Everything from first tracker")
    }
    
    public func sayHi(_ name: String) {
        print("Hi \(name) from first tracker")
    }
    
    public func sayBye(_ name: String) {
        print("Goodbye \(name) from first tracker")
    }
    
    
}

public class SecondTracker: Tracker {
    public init(){}
    public func printEverything() {
        print("Everything from second tracker")
    }
    
    public func sayHi(_ name: String) {
        print("Hi \(name) from second tracker")
    }
    
    public func sayBye(_ name: String) {
        print("Goodbye \(name) from second tracker")
    }
}


public class MainTracker: Tracker{
    public static let shared: MainTracker = MainTracker()
    private var firstTracker: Tracker!
    private var secondTracker: Tracker!
    
    public func set(firstTracker: Tracker, secondTracker: Tracker){
        self.firstTracker = firstTracker
        self.secondTracker = secondTracker
    }
    
    public func printEverything() {
        firstTracker.printEverything()
    }
    
    public func sayHi(_ name: String) {
        secondTracker.sayHi(name)
    }
    
    public func sayBye(_ name: String) {
        firstTracker.sayBye(name)
    }

}
