//
//  ViewController.swift
//  AdapterPatternSwift
//
//  Created by Ihwan on 08/07/21.
//

import UIKit
import Tracker

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        MainTracker.shared.printEverything()
        MainTracker.shared.sayHi("Ihwan")
        MainTracker.shared.sayBye("Ihwan")
    }


}

